import xml.etree.ElementTree as ET
import boto
import tornado.ioloop
import tornado.web
import datetime
import simplejson as json
import cPickle as pickle
import os
import time
from datetime import date

from kafka.client import KafkaClient
from kafka.producer import SimpleProducer
from kafka.consumer import SimpleConsumer

from kafka import KafkaConsumer

from pydruid.client import *

  # more advanced consumer -- multiple topics w/ auto commit offset
  # management
#consumer = KafkaConsumer('http-connections',
#                           bootstrap_servers=['192.168.122.135:9092'],
#                           group_id='my_consumer_group',
#                           auto_commit_enable=True,
#                           auto_commit_interval_ms=30 * 1000,
#                           auto_offset_reset='smallest')


print("After connecting to kafka")

query = PyDruid('http://192.168.122.228:8080', 'druid/v2/')

print("After connecting to druid")

kafka =  KafkaClient("192.168.122.135:9092")
#consumer = SimpleConsumer(kafka, "my-group", "my-topic")

#s3 = boto.connect_s3()
#print("After connecting to S3")

running_count = 0
class JackylApiHandler(tornado.web.RequestHandler):
      def post(self):
          content = self.request.body
          content_size = str(len(content))
          print content_size
          self.write(content_size)
          file_name = "/home/tornado/storage/%s.json" % datetime.datetime.utcnow().isoformat()
          with open(file_name, 'w') as fp:
               #pickle.dump(d, fp)
               json.dump(content, fp)
               #file_size = os.path.getsize(file_name)
               #self.write('%s' % file_size)
               fp.close()
               print "%s has been written to storage" % file_name


class KafkaProducerHandler(tornado.web.RequestHandler):

      topic = "jackyl"
      global producer
      producer = SimpleProducer(kafka)
  
      def post(self):
      #def get(self):
          #time.sleep(15)
          #s3 = boto.connect_s3()
          print("After connecting to S3")

          key_sent = self.request.body
          print key_sent
          
          key_size = str(len(key_sent))
          print key_size
          self.write(key_size)
          nfile = key_sent.replace("'","\"")
          #print nfile
          #strtojson = json.loads(nfile)
          #print strtojson
          jsonfile = json.loads(nfile)
          print jsonfile
          aws_file = jsonfile['FileName']
          print aws_file 
          ## Connect to s3, go to specified bucket and save xml file
          #s3 = boto.connect_s3()
          #aws_bucket = s3.get_bucket('lloopplogs')
          #aws_bucket.list()
          # Create a new key/value pair.
          #key = aws_bucket.new_key('test-key')
          #key.set_contents_from_string("Hello World!")
          #key.set_contents_from_string(xmlstring)
          # Sleep to ensure the data is eventually there.
          #time.sleep(2)

          #aws_key = aws_bucket.get_key(aws_file)

          # Retrieve the contents of ``test-key``.
          #aws_content = aws_key.get_contents_as_string()
          #print aws_content

          #self.write(aws_content)

          # Send xml string to Kafka topic
          #topic = "jackyl"
          #producer.send_messages(topic, aws_content)

          # Save contents to storage
          #pickle/json the data and save to storage
          #xmlfile = "/home/tornado/storage/%s.xml" % datetime.datetime.utcnow().isoformat()
          #with open(file_name, 'w') as fp:
               #pickle.dump(d, fp)
          #     json.dump(aws_content, fp)
          #     file_size = os.path.getsize(file_name)
          #     self.write('%s' % file_size)
               #fp.close()
          #tree = ET.ElementTree(ET.fromstring(aws_content))
          #tree.write(xmlfile)

class DruidHandler(tornado.web.RequestHandler):
 def set_default_headers(self):
     self.set_header("Access-Control-Allow-Origin", "http://184.107.228.26:8084")

 def get(self): 
     top = query.topn(
            datasource='top-pages',
            granularity='all',
            intervals='2012-10-01T00:00/2020-01-01T00',
            aggregations={"count": doublesum("count")},
            dimension='url',
            metric='count',
            threshold=10
        )

     print json.dumps(query.query_dict, indent=2)
     results = json.dumps(query.result, indent=2) 
     print query.result
     self.write(results)

class KafkaConsumerHandler(tornado.web.RequestHandler):
 #consumer = SimpleConsumer(kafka, "my-group", "my-topic")

 stored_date = str(datetime.date.today())
 print "Stored Date: ", stored_date
 running_count = 0 
 print "Running Count: ", running_count

 def set_default_headers(self):
     self.set_header("Access-Control-Allow-Origin", "http://184.107.228.26:8084") 

 def show_result(self, response):
     print response
     self.write(response)
     kafka.close()

 def get(self):
     global running_count
     response = {}
     response["ip_count"] = []
     response["running_count"] = []
     var = 1
    
     while var > 0:
        for m in consumer.fetch_messages():
            var = var -1
            print m
            print m.value
            ip_count = m.value
            print "Global Count: ", running_count
            #running_count = running_count + int(ip_count)
            print "Running Count: ", running_count
            response["ip_count"].append(ip_count)
            #response["running_count"].append(running_count)
            consumer.task_done(m)
            if var == 0:
               break
        consumer.commit()
     print response   
     self.write(response)
         #self.show_result(response)
         #return response
  
application = tornado.web.Application([
(r"/aws", JackylApiHandler),
(r"/inbound", KafkaProducerHandler),
(r"/consumer", KafkaConsumerHandler),
(r"/topurl", DruidHandler)
])

if __name__ == "__main__":
   application.listen(9998)
tornado.ioloop.IOLoop.instance().start()
